# Atlassian Webpack Soy Loader
A soy loader for webpack that has Atlassian's custom functions available like `getText` and `contextPath`.

## Installation
You will need to have access to the Atlassian private npm registry, login with your staffId credentials by running this command:

```
npm login --registry=https://npm-private.atlassian.io --scope=atlassian
```

Then run:

```
npm install @atlassian/atlassian-webpack-soy-loader
```

Add the following entries to your webpack config:

```
 module: {
        loaders: [
            {
                test: /\.soy$/,
                loader: '@atlassian/atlassian-webpack-soy-loader'
            }
        ]
    },
    soyLoader: {
        namespaces: ["<top-level-namespace>"]
    }
```

The `namespaces` option is required. This must be an array of your top level namespaces.  For example if you have the following namespaces in your code

```
//someFile.soy
{namespace ServiceDesk}

//someOtherFile.soy
{namespace ServiceDesk.Pages}

//someOtherFile2.soy

{namespace Jira.Test}
```

You namespaces array should be: `["ServiceDesk", "Jira"]`

This is needed because soy still needs to compile to a global, however after webpack wraps our soy code inside functions they are no longer global.

The namespaces array is used to replace those declarations so they still hang off the window and can be accessed by soy itself.

##Options
These options can be placed in the `soyLoader` object inside your webpack config.


| Option        | Description | Default |
| ------------- |-------------|---------|
| jar    | You can define a path (relative to your project directory) to your own version of the jar to use when compiling soy | undefined (uses built-in jar if this isn't passed in) |
| namespaces    | array top-level namespaces to ensure still hang off the global | undefined |
| functions | Absolute path of a JAR containing custom Soy functions | undefined |

